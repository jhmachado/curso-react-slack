import React from 'react';
import { Progress } from 'semantic-ui-react';

class ProgressBar extends React.Component {

  render() {
    const { uploadState, percentUploaded } = this.props;

    if (uploadState !== 'uploading') {
      return <div></div>;
    }

    return (
      <Progress
        className="progress__bar"
        percent={percentUploaded}
        progress
        indicating
        size="medium"
        inverted
      />
    )
  }
}

export default ProgressBar;
