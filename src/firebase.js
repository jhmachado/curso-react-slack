import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCMAL4LpXkhCzYZyST0ylI_K9banQ4pKNs",
  authDomain: "pwa-chat-c96f2.firebaseapp.com",
  databaseURL: "https://pwa-chat-c96f2.firebaseio.com",
  projectId: "pwa-chat-c96f2",
  storageBucket: "pwa-chat-c96f2.appspot.com",
  messagingSenderId: "425935925180",
  appId: "1:425935925180:web:e145136cad7f2978"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
